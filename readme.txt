
********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Block Bar Module
Author: der <dreed10 -at- gmail -dot- com>
Drupal: 5.x
********************************************************************
DESCRIPTION:

The BlockBar module is a block module that acts as a container for other 
Drupal blocks.  The module builds an Outlook style collapsible menu
bar from the child blocks with each child block representing one panel
within the bar.  Multiple BlockBars can be created.  Each BlockBar can 
be set with "sticky panels" which will remember the panel's open/close 
state between page refreshes.  Also, an option can be set that will only 
allow one panel to be open at a time for a given bar.  

NOTE! *****
This module is designed to work with Drupal 4.7 and later releases, it 
will NOT work in 4.6 or earlier.

UPDATE *****
I have a version of this module working for 5.x.  At this point I would
consider i beta at best.

********************************************************************
INSTALLATION:

1. Create folder 'modules/blockbar'

2. Copy all the modules files, keeping directory structure, to the 
folder 'modules/blockbar'

3. Enable the blockbar module in 'admin/build/modules'

********************************************************************
CONFIGURATION:

** NOTE ** - This module takes advantage of the new Regions capability 
available in 4.7 and later version of Drupal.

These are the relevant steps.

1. Create a "container region" in your template.php.  The sole purpose 
of this new region is to act as a container for your blockbar elements 
(ie normal blocks)

2. Enable the blockbar module and add at least 1 blockbar block in 
admin/settings/blockbar

3. Click "configure" in admin/block for your new blockbar and set the 
region dropdown to the new container region you created in step 1.  
Then click "save block"

4. Enable your new blockbar block to be shown where you want it in 
admin/block.  For example, if you want to show your new blockbar on 
the left sidebar then set the "placement" dropdown to "left sidebar".

5. The only thing left is for you to assign any normal blocks to your 
new "container" region.  Essentually, you're telling Drupal which 
normal blocks you want to show up in your blockbar.  So, for example,
if you want the "navigation" block to show up in the blockbar just 
change it's placement from the default "left sidebar" to your new 
container region you defined in step one.


Essentually you will need to build a structure like this:

[display region] 
    |
    +---- [BlockBar block]
              |
              +----- [container region]
                         |
                         + ---- [drupal block 1]
                         + ---- [drupal block 2]
                         + ---- etc ...

[display region] - This is one of the standard regions (e.g. left sidebar, 
right sidebar, content ....)

[BlockBar block] - This is the container BlockBar block

[container region] - This is the source region for this BlockBar. This
region cannot be one of the standard regions (left sidebar, right sidebar etc)
This must be a new region that you setup just to act as a container
for this BlockBar.  See http://drupal.org/node/29140 for detailed
information on how to define new custom regions.  The short version, just add
the following function to your template.php file (or mytheme.theme file depending
on your theme).  The last 2 regions in the list below are your new regions.  The 
first five are the standard Drupal regions.

function my_theme_regions() {
  return array(
       'left' => t('left sidebar'),
       'right' => t('right sidebar'),
       'content' => t('content'),
       'header' => t('header'),
       'footer' => t('footer'),
       'blockbar_container_1' => t('blockbar_container_1'),
       'blockbar_container_2' => t('blockbar_container_2')
  );
}   

[drupal block 1] - This is a normal drupal block such as 'Navigation',
'User Login', 'Who's Online' etc.


********************************************************************
  
       
